package common

import (
	"io"
	"net"
	"sync/atomic"
	"time"
)

var connId uint32 = 1

const INVALIED_STUB_ID uint32 = 0

func IsNoDataError(err error) bool {
	netErr, ok := err.(net.Error)
	//抄的官方包 调用Temporary方法判断这个错误是否为临时的错误，然后决断是退出服务还是过一段时间后重新尝试Accept
	if ok && netErr.Timeout() && netErr.Temporary() {
		return true
	}
	return false
}

func IsNetOpError(err error) bool {
	if _, ok := err.(*net.OpError); ok {
		return true
	}
	return false
}

func IsNetEofError(err error) bool {
	if err == io.EOF {
		return true
	}
	return false
}

//获取ConnID
func GetConnID() uint64 {
	count := atomic.AddUint32(&connId, 1)
	second := uint32(time.Now().Unix())

	return uint64(second<<32) + uint64(count)
}
