package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func PathExits(path string) bool {
	finfo, err := os.Stat(path)
	if err != nil {
		return os.IsExist(err)
	}

	return finfo.IsDir()
}

func FileExits(file string) bool {
	finfo, err := os.Stat(file)
	if err != nil {
		return os.IsExist(err)
	}

	return !finfo.IsDir()
}

func DealInputPath(path string) (string, error) {
	path, err := filepath.Abs(path)
	if err != nil {
		fmt.Printf("Convert %s to Abs path err %v", path, err)
		return "", err
	}

	path = strings.Replace(path, "\\", "/", -1)
	if path[len(path)-1] == '/' {
		path = path[:len(path)]
	}
	return path, nil
}
