import json
import sys
import os

class FileGenerator:
    def __init__(self, file_name, pb_json = None):
        self.file_name = file_name
        self.file_name_split = file_name.split(".")
        self.pb_json = pb_json
        self.gen_struct_pb_file()

    def gen_struct_pb_file(self):
        file = open(self.file_name_split[0] + ".service.proto", "w")
        file.write("// Machine generated code\n\n")
        file.write('syntax = "proto3";\n\n')
        file.write("package " + self.file_name_split[0] + ";\n\n")
        file.write('option go_package="idldata/pbdata";\n\n')


        for struct in self.pb_json["structs"]:
            file.write("message " + struct["name"] + " {\n")
            n = 1
            for field in struct["fields"]:
                if field["IdlType"] == "dict":
                    file.write("    " + field["type"] + "<" + field["key"]["type"] + "," +   field["value"]["type"] + "> " + field["name"] + " = " + str(n) + ";\n")
                elif field["IdlType"] == "seq" or field["IdlType"] == "set":
                    file.write("    " + field["type"] + " " + field["key"]["type"] + " " + field["name"] + " = " + str(n) + ";\n")
                else:
                    file.write("    " + field["type"] + " " + field["name"] + " = " + str(n) + ";\n")
                n += 1
            file.write("}\n\n")
        self.gen_method_pb_file(file)
        self.gen_method_ret_pb(file)
        file.close()


    def gen_method_ret_pb(self, file):
        for service in self.pb_json["services"]:
            for method in service["methods"]:
                file.write("message " + service["name"] + "_" + method["name"] + "_ret {\n")
                if method["retType"]["IdlType"] == "dict":
                    file.write("    " + method["retType"]["type"] + "<" + method["retType"]["key"]["type"] + "," +   method["retType"]["value"]["type"] + "> " + " ret1 = 1;\n")
                elif method["retType"]["IdlType"] == "seq" or method["retType"]["IdlType"] == "set":
                    file.write("    " + method["retType"]["type"] + " " + method["retType"]["key"]["type"] + " ret1 = 1;\n")
                elif method["retType"]["IdlType"] == "void":
                    pass
                else:
                    file.write("    " + method["retType"]["type"] + " ret1 = 1;\n")
                file.write("}\n\n")


    def gen_method_pb_file(self, file):
        for service in self.pb_json["services"]:
            for method in service["methods"]:
                file.write("message " + service["name"] + "_" + method["name"] + "_args {\n")
                n = 1
                for arg in method["arguments"]:
                    if arg["IdlType"] == "dict":
                        file.write("    " + arg["type"] + "<" + arg["key"]["type"] + "," +   arg["value"]["type"] + ">")
                    elif arg["IdlType"] == "seq" or arg["IdlType"] == "set":
                        file.write("    " + arg["type"] + " " + arg["key"]["type"])
                    elif arg["IdlType"] == "void":
                        continue
                    else:
                        file.write("    " + arg["type"])
                    file.write(" arg" + str(n) + " = " + str(n) + ";\n") 
                    n += 1
                file.write("}\n\n")


if __name__ == "__main__":
    pbjson = json.load(open(sys.argv[2]))
    FileGenerator(sys.argv[1], pbjson)