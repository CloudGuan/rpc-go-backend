package service

//@title 必须实现的接口
type Service interface {
	GetUUID() uint64
	GetInstanceID() uint32
	GetName() string
	Dispatch() error
}

//@title service 包装类
type serviceBase struct {
	servImpl Service //服务实现
}

func NewService() *serviceBase {
	return nil
}
