package net

//都在主线程调度

//@titile 监听回调接口
type AcceptHandle interface {
	OnAccept(conn IdlConn) bool //主动监听收到对应事件
}

//@title 连接回调接口
type ConnHandle interface {
	OnConnect(coon IdlConn) bool //主动连接成功得逻辑
	OnRecv(pkg []byte) bool      //逻辑协程，从网络协程收包
	//	OnRead(pkg []byte) bool      //网络协程, 从逻辑协程读包
	OnClose() //客户端连接断开，清理善后
}
