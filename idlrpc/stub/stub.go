package stub

import (
	"gitee.com/CloudGuan/rpc-go-backend/idlrpc/protocol"
	"gitee.com/CloudGuan/rpc-go-backend/idlrpc/transport"
)

const (
	SERVICE_RESOLVED = iota + 1
	SERVICE_UPDATING
)

//@title stub 接口
type SrvStub interface {
	GetUUID() uint64
	GetServiceName() string
	GetSignature(metchid uint32) string
	MultipleNum() uint32
	IsOneWay(uint32) bool
	RegisterImpl(interface{})
	CallMethod(*protocol.RequestPackage) (*protocol.ResponsePackage, error)
}

type ProxyStub interface {
	//@title 获取proxyid
	GetSrvUUID() uint64
	//@title 获取服务名称
	GetSrvName() string
	//@title 获取函数签名
	GetSignature(uint32) string
	//@title 获取某个函数是否需要返回值
	IsOneWay(uint32) bool
	//@title 设置Proxy绑定得stub得实例id
	SetClientID(uint32)
	//@title 获取proxy的Trans
	GetTransport() transport.Transport
}
