package transport

const (
	TRANS_WORKING = iota
	TRANS_CLOSED
)

type Transport interface {
	Wirte(pkg []byte, length int) (int, error) //往缓冲区写入
	Read(pkg []byte, length int) (int, error)  //从缓冲区读取，并且移除数据
	Peek(length int) ([]byte, int, error)      //获取头部的定长数据 但是不移除他们
	Send(pkg []byte) error                     //发送数据包 多协程调用
	Close()                                    //关闭
	Size() uint32                              //返回读队列长度
	IsClose() bool                             //查询状态
	GetID() uint32                             //获取Trans ID
	SetID(transID uint32)                      //设置Trans ID
}
