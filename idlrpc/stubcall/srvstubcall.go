package stubcall

import (
	"gitee.com/CloudGuan/rpc-go-backend/idlrpc/protocol"
	"gitee.com/CloudGuan/rpc-go-backend/idlrpc/transport"
)

//@title 远端调用存根，用于调度
type SrvStubCall struct {
	serviceUID    uint64
	methodID      uint32                   //调用方法id
	serviceID     uint32                   //服务实例 go语言版本 只有一哥
	clientCallID  uint32                   //客户端调用存根Id 由协议头赋值
	serviceCallID uint32                   //服务器调用存根Id
	reqmsg        *protocol.RequestPackage //请求包
	trans         transport.Transport      //transport
}

func NewSrvStubCall(serviceUUID uint64, srvID, callID, methodID uint32, trans transport.Transport) *SrvStubCall {
	return &SrvStubCall{
		serviceUID:   serviceUUID,
		methodID:     methodID,
		serviceID:    srvID,
		clientCallID: callID,
		trans:        trans,
	}
}

//客户端存根id
func (sc *SrvStubCall) GetClientCallID() uint32 {
	return sc.clientCallID
}

func (sc *SrvStubCall) GetMethodID() uint32 {
	return sc.methodID
}

func (sc *SrvStubCall) GetServiceID() uint32 {
	return sc.serviceID
}

//服务器存根id
func (sc *SrvStubCall) GetServerCallID() uint32 {
	return sc.serviceCallID
}

func (sc *SrvStubCall) GetTransportID() transport.Transport {
	return sc.trans
}

func (sc *SrvStubCall) GetReqData() *protocol.RequestPackage {
	return sc.reqmsg
}

func (sc *SrvStubCall) SetReqData(req *protocol.RequestPackage) {
	sc.reqmsg = req
}
